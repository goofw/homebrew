class SingBox < Formula
  desc "The universal proxy platform"
  homepage "https://github.com/SagerNet/sing-box"
  version "v1.10.7"
  url "https://gitlab.com/goofw/app/-/releases/v1.10.7/downloads/sing-box-darwin-amd64.tar.gz"
  license "GPL-3.0-or-later"

  def install
    bin.install "sing-box"
  end

  test do
    system "#{bin}/sing-box", "version"
  end
end
