class Naiveproxy < Formula
  desc "Make a fortune quietly"
  homepage "https://github.com/klzgrad/naiveproxy"
  version "v133.0.6943.49-1"
  url "https://github.com/klzgrad/naiveproxy/releases/download/v133.0.6943.49-1/naiveproxy-v133.0.6943.49-1-mac-x64.tar.xz"
  license "BSD"

  def install
    bin.install "naive"
  end

  test do
    system "#{bin}/naive", "--version"
  end
end
